Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :films, only: [] do
        collection do
          get :get_films
          get :get_film_for_year
          get :get_cached_dates
        end
      end
    end
  end

  resources :films, only: [] do
    collection do
      get :clear_cache
    end
  end

  root "films#film_view"
end

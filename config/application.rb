require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Yipittakehomechallenge
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end

if ENV["REDISTOGO_URL"]
  config = Yipittakehomechallenge::Application.config
  uri = URI.parse(ENV["REDISTOGO_URL"])

  config.cache_store = [
    :redis_store, {
      :host => uri.host,
      :port => uri.port,
      :password => uri.password,
      :namespace => "cache"
    }
  ]
end

require 'spec_helper'

describe Films::Parser do
  let(:response_object0) { double("response") }
  let(:response_object1) { double("response") }
  let(:params) {
    {
      "results": [
        {
          "films": [
            {
                "Detail URL": "test_url_0",
                "Film": "Wings",
                "Winner": true
            },
            {
                "Detail URL": "test_url_2",
                "Film": "The Racket",
                "Winner": false
            }
          ],
          "year": "1927"
        },
        {
        "films": [
            {
                "Detail URL": "test_url_1",
                "Film": "The Broadway Melody",
                "Winner": true
            }
          ],
        "year": "1928"
        }
      ]
    }
  }

  let(:test_film_0_details) {
    { "Budget": "$1.3 million [ 1 ]", "Title": "Wings" }
  }

  let(:test_film_1_details) {
    { "Budget": "$3.2 million [ 1 ]", "Title": "The Broadway Melody" }
  }

  let(:parser) { Films::Parser.new(params) }

  describe '#run' do
    before do
      REDIS.flushall
      allow(Films::ApiRequest).to receive(:run).with("test_url_0").and_return(response_object0)
      allow(Films::ApiRequest).to receive(:run).with("test_url_1").and_return(response_object1)
      allow(REDIS).to receive(:set).with(an_instance_of(String), an_instance_of(String)).and_return("OK")
      allow(REDIS).to receive(:set).with(an_instance_of(String), an_instance_of(String)).and_return("OK")
      allow(response_object0).to receive(:body).and_return(test_film_0_details.to_json)
      allow(response_object1).to receive(:body).and_return(test_film_1_details.to_json)

      @result = parser.run
    end

    it "stores in redis" do
      expect(REDIS).to have_received(:set).exactly(2).times
    end

    it "returns the array of json in appropriate format" do
      # Result should be of the structure:
      # {
      #   details: {
      #     [
      #       {year: 1927, title: "Wings", budget: 1300000},
      #       {year: 1927, title: "The Racket", budget: 2100000},
      #       {year: 1928, title: "The Broadway Melody", budget: 3200000}
      #     ]
      #   },
      #   average_winning_budget: 22000000
      # }

      expect(@result["details"][0]["year"]).to eq(1927)
      expect(@result["details"][0]["title"]).to eq("Wings")
      expect(@result["details"][0]["budget"]).to eq(1300000)

      expect(@result["details"][1]["year"]).to eq(1928)
      expect(@result["details"][1]["title"]).to eq("The Broadway Melody")
      expect(@result["details"][1]["budget"]).to eq(3200000)

      expect(@result["details"][2]).to be(nil)

      expect(@result["average_winning_budget"]).to eq(2250000)
    end
  end
end

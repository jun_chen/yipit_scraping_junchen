require "rails_helper"

RSpec.describe Api::V1::FilmsController, :type => :controller do
  let(:parser_obj) { double("parser") }
  let(:http_response) { Net::HTTPResponse.new(1.0, 200, "OK") }

  describe "#get_films" do
    before do
      allow(Films::Parser).to receive(:new).and_return(parser_obj)
      allow(parser_obj).to receive(:run).and_return({})
      allow(Films::ApiRequest).to receive(:run).and_return(http_response)
      allow(http_response).to receive(:body).and_return("{}")

      get :get_films
    end

    it "calls the request service" do
      expect(Films::ApiRequest).to have_received(:run)
    end

    it "instantiates and runs the parser" do
      expect(Films::Parser).to have_received(:new).with({})
      expect(parser_obj).to have_received(:run)
    end

    it "returns type json" do
      expect(response.content_type).to eq 'application/json'
    end

    context "when the response is successful" do
      it "responds with the success code" do
        expect(response.status).to eq 200
      end
    end

    context "when the response has an error" do
      let(:http_response) { Net::HTTPResponse.new(1.0, 400, "Error") }

      it "responds with the error code" do
        expect(response.status).to eq 400
      end
    end
  end
end

#!/usr/bin/ruby
require "./config/environment"

def make_request
    puts "Alrighty, here we go: "
    puts

    response = Films::ApiRequest.run
    response_json = JSON.parse(response.body)
    output = Films::Parser.new(response_json).run(true, ARGV[0], ARGV[1])

    puts "Average winning budget: " + output["average_winning_budget"].to_s
    puts
    puts "Hope that was helpful! Feel free to visit https://yipit-api-take-home.herokuapp.com/ for a User Interface."
end

make_request

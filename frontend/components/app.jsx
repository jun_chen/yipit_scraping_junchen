import React from 'react';
import { Provider } from 'react-redux';
import {
  Route,
  Redirect,
  Switch,
  Link,
  HashRouter
} from 'react-router-dom';

import FilmView from './film_view';

const App = () => (
  <div>
    <header>
      <Link to="/" className="header-link">
        <h1>Films API</h1>
      </Link>
    </header>
    <Switch>
      <Route exact path="/" component={FilmView} />
    </Switch>
  </div>
);

export default App;

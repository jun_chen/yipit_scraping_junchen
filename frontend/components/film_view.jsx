// With more time, I would refactor these class components into their own separate files and import as appropriate

import React from 'react';
import { withRouter } from 'react-router';
import Modal from 'react-modal';
import moment from 'moment';
import ReactLoading from 'react-loading';

class FilmView extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      error: "",
      data: [],
      modalOpen: false,
      loadingModalOpen: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEnterPress = this.handleEnterPress.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.openModal = this.openModal.bind(this);
  }

  handleEnterPress(event){
    if(event.charCode == 13) this.handleSubmit();
  }

  handleSubmit() {
    const _this = this;
    this.setState({loadingModalOpen: true});
    $.ajax({
      type: "GET",
      url: "/api/v1/films/get_films",
      success: (resp) => {
        this.setState({loadingModalOpen: false});
        _this.searchSuccessCallback(resp)
      },
      error: (err) => {
        this.setState({loadingModalOpen: false});
        _this.searchErrorCallback(err)
      }
    })
  }

  searchSuccessCallback(resp) {
    if (resp) {
      if(resp["status"] == 418) {
        this.setState({error: resp["error"]})
      } else {
        this.setState({error: ""})
      }
      this.setState({data: resp["data"], showWarning: resp["warning_has_more"], resultsVisible: true});
    }
    else {
      this.setState({error: "Error while retrieving data", resultsVisible: false})
    }
  }

  searchErrorCallback(err)  {
    this.setState({error: err})
  }

  closeModal() {
    this.setState({modalOpen: false})
  }

  openModal() {
    this.setState({modalOpen: true})
  }

  render() {
    return (
      <div id="main">
        <div className="ui form date">
          <div className="input-wrapper">
            <div className="gap-top">
              <button className="ui button primary" onClick={this.handleSubmit}>Get Data</button>
            </div>
          </div>
          <ErrorMessage message={this.state.error} />
          <div className="gap-top"></div>
          <div className="align-middle">
            <button className="ui button secondary" onClick={this.openModal}>View Cached Dates</button>
          </div>
          <div className="gap-top"></div>
        </div>

        { this.state.resultsVisible &&
          <ResultsTable data={this.state.data} showWarning={this.state.showWarning}/>
        }

        <Modal
          isOpen={this.state.loadingModalOpen}
          style={{content : { border: 'none', top : '40%', left : '45%', height: '159px', width: '120px' }}}
        >
          <div className="margin-middle">
            <ReactLoading type={"cylon"} color={"#000000"} height='100' width='100' delay={300} />
          </div>
        </Modal>

        <Modal
          isOpen={this.state.modalOpen}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Cached Dates"
        >
          <FilmModal/>
          <div className="gap-top"></div>
          <div className="align-middle">
            <button className="ui button secondary" onClick={this.closeModal}>Close</button>
          </div>
        </Modal>
      </div>
    );
  }
}

const customStyles = {
  overlay : {
    position          : 'fixed',
    top               : 0,
    left              : 0,
    right             : 0,
    bottom            : 0,
    backgroundColor   : 'rgba(255, 255, 255, 0.75)'
  },
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    height                : '60%',
    width                 : '60%'
  }
}

class FilmModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: "",
      data: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClear = this.handleClear.bind(this);
  }

  handleSubmit() {
    const _this = this;
    $.ajax({
      type: "GET",
      url: "/api/v1/films/get_cached_dates",
      success: (resp) => {
        _this.searchSuccessCallback(resp)
      },
      error: (err) => {
        _this.searchErrorCallback(err)
      }
    })
  }

  handleClear() {
    const _this = this;
    $.ajax({
      type: "GET",
      url: "/films/clear_cache",
      success: (resp) => {
        _this.searchSuccessCallback(resp)
      },
      error: (err) => {
        _this.searchErrorCallback(err)
      }
    })
  }

  searchSuccessCallback(resp) {
    if (resp) {
      this.setState({data: resp["data"], showWarning: resp["warning_has_more"], resultsVisible: true});
    }
    else {
      this.setState({error: "Error appears while retriving data!", resultsVisible: false})
    }
  }

  searchErrorCallback(err)  {
    this.setState({error: err})
  }

  render() {
    var rows = [];

    if (this.state.data && this.state.data.length > 0) {
      this.state.data.forEach((result) => {
        rows.push(<tr> <td> {result} </td> </tr>);
      })
    } else {
        rows.push(<tr><td colSpan="6" key="nmrf">No results found</td></tr>)
    }

    return (
      <div className="ui form">
        <div className="">
          <div className="align-middle">
            <button className="ui button primary" onClick={this.handleSubmit}>Get dates</button>
            <button className="ui button secondary" onClick={this.handleClear}>Clear cache</button>
          </div>
        </div>
        <ErrorMessage message={this.state.error} />
        { this.state.resultsVisible &&
          <table className="result-index ui basic table striped">
            <thead>
              <tr>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
              {rows}
            </tbody>
          </table>
        }
      </div>
    );
  }
}


class ResultsTable extends React.Component {
  render() {
    var rows = [];

    if (this.props.data.details.length > 0) {
      this.props.data.details.forEach((result) => {
        rows.push(<ResultRow result={result} />);
      })
    } else {
        rows.push(<tr><td colSpan="6" key="nmrf">No results found</td></tr>)
    }

    return (
      <div className="">
        <Warning visible={this.props.showWarning}/>
        <table className="result-index ui basic table striped">
          <thead>
            <tr>
              <th>Year</th>
              <th>Title</th>
              <th>Budget</th>
            </tr>
          </thead>
          <tbody>
            {rows}
          </tbody>
        </table>

        <table className="result-index ui basic table striped">
          <thead>
            <tr>
              <th>Average Budget of Winning Films</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{this.props.data.average_winning_budget}</td>
            </tr>
          </tbody>
        </table>

        <Warning visible={this.props.showWarning}/>
      </div>
    );
  }
}

class ResultRow extends React.Component {
  render() {
    return (
      <tr>
        <td>{this.props.result.year}</td>
        <td>{this.props.result.title}</td>
        <td>{this.props.result.budget}</td>
      </tr>
    );
  }
}

class Warning extends React.Component {
  render() {
    if (!this.props.visible) {
      return null;
    }
    else {
      return (
        <div className="ui visible warning yellow message">
          Warning!  Based on your search, there are more results than displayed here. Please enter more info to narrow your results.
        </div>
      )
    }
  }
}

class ErrorMessage extends React.Component {
  render() {
    if (this.props.message.length === 0) {
      return null;
    }
    else {
      return (
        <div className="ui visible error message">
          Error! { this.props.message }
        </div>
      )
    }
  }
}

export default withRouter(FilmView);

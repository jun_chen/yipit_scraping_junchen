class FilmsController < ApplicationController
  def clear_cache
    REDIS.flushall
    render json: { status: 200, data: REDIS.keys }
  end
end

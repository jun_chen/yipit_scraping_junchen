class Api::V1::FilmsController < ActionController::Base
  def get_films
    response = Films::ApiRequest.run
    response_json = JSON.parse(response.body)
    output = Films::Parser.new(response_json).run

    render json: { data: output }, status: response.code.to_i
  end

  def get_film_for_year
    film_details = {}
    if REDIS.exists(params[:year])
      film_details = JSON.parse(REDIS.get(params[:year]))
      film_details["year"] = params[:year]
    else
      response = Films::ApiRequest.run
      response_json = JSON.parse(response.body)
      film_details = get_details_for_year(response_json)
    end

    render json: { data: film_details }, status: 200
  end

  def get_cached_dates
    render json: { data: REDIS.keys }, status: 200
  end

  private

  def get_details_for_year(response_json)
    film_details = {}
    response_json["results"].each do |result|
      parsed_year = Films::Parser.parse_year(result["year"])

      # Find the data structure that points to the params year
      next unless parsed_year == params[:year].to_i
      film_details = Films::Parser.new.parse_films_for_year(result, parsed_year)
      break
    end
    film_details
  end
end

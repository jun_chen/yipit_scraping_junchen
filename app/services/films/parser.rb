class Films::Parser
  NUMBER_STRING_CONVERSION = {
    "billion" => 1000000000,
    "million" => 1000000,
    "thousand" => 1000
  }

  def self.parse_budget(raw_string)
    result = 1
    char_arr = raw_string.split(/[[:space:]]/)
    char_arr.each_with_index do |char, idx|
      if NUMBER_STRING_CONVERSION[char]
        # Tradeoff: this precludes parsing any data after one of the keywords ("million", etc)
        result *= NUMBER_STRING_CONVERSION[char]
        break
      elsif idx == 0
        # Account for dash/range of numbers case, e.g. "$1-2 million"
        nums_arr = char.match(/[\d]+\-?[\d]+/)&.string&.split("-")&.map {|el| el.gsub(/[^(\d|\.)]/, '')}
        if nums_arr && nums_arr.length == 2
          avg = (nums_arr[0].to_f + nums_arr[1].to_f) / 2
          cleaned_char = avg
        else
          cleaned_char = char.gsub(/[^(\d|\.)]/, '')
        end

        result *= cleaned_char.to_f if cleaned_char.present?
      else
        break if char == "[" || char == "("
        cleaned_char = char.gsub(/[^(\d|\.)]/, '')
        result *= cleaned_char.to_f if cleaned_char.present?
      end
    end
    result.to_i
  end

  def self.parse_year(raw_string)
    # Captures the first four digits
    raw_string[/^\d{4}/].to_i
  end

  def initialize(params = {})
    @params = params.with_indifferent_access
  end

  def run(script_execution = false, start_year = nil, end_year = nil)
    output = {}
    film_count = 0
    total_winning_budget = 0
    winning_films = []

    @params["results"].each do |result|
      year = self.class.parse_year(result["year"])

      # If a range of years is provided, then skip current year unless within range
      if start_year && end_year
        next if year > end_year.to_i || year < start_year.to_i
      end

      film_details = {}
      if REDIS.exists(year.to_s)
        film_details = JSON.parse(REDIS.get(year.to_s))
        film_details["year"] = year
      else
        film_details = parse_films_for_year(result, year)
      end
      winning_films << film_details

      if film_details["budget"].present?
        film_count += 1
        total_winning_budget += film_details["budget"]
      end

      print_details(film_details) if script_execution
    end

    output["details"] = winning_films
    output["average_winning_budget"] = (total_winning_budget / film_count).to_i unless film_count == 0
    output
  end

  def parse_films_for_year(result, year)
    winning_film_details = {}
    result["films"].each do |film|
      if film["Winner"] == true
        response = Films::ApiRequest.run(film["Detail URL"])
        film_detail_response = JSON.parse(response.body)

        budget = self.class.parse_budget(film_detail_response["Budget"]) if film_detail_response["Budget"]
        title = film_detail_response["Title"]

        REDIS.set(year.to_s, { title: title, budget: budget }.to_json)
        winning_film_details = { year: year, title: title, budget: budget }.with_indifferent_access
      end
    end
    winning_film_details
  end

  private

  def print_details(details)
      print "Year: " + details["year"].to_s
      print ", "
      print "Title: " + details["title"]
      puts
      print "Budget: " + details["budget"].to_s
      puts
      puts
  end
end

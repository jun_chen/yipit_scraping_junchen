require 'open-uri'
require 'net/http'

class Films::ApiRequest
  def self.run(url = "http://oscars.yipitdata.com")
    uri = URI(url)
    headers =
    {
      'Content-Type' => "application/json"
    }

    request = Net::HTTP::Get.new(uri.request_uri)
    response = Net::HTTP.start(uri.host, uri.port) {|http|
      http.request(request)
    }
  end
end

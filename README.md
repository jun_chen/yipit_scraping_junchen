# Yipit API Take-Home Challenge

This project uses the Yipit API to parse film-related wikipedia data.

Average winning budget: $17,948,087

## Setup
(May go [here](https://yipit-api-take-home.herokuapp.com/) for a live site)

** Script execution:

* Install Ruby interpreter somewhere in your path
* `cd` into project directory
* In your terminal console, enter `ruby script.rb`
* Note: data for a range of dates can be retrieved via `ruby script.rb start_year_here end_year_here`

** For local development of web service:

* `cd` into project directory
* `bundle install`
* `rake db:create`
* `rake db:migrate`
* `npm install`
* Launch `rails server`

## Tests

* Basic tests for the MVP functionality are provided. Run `rake db:test:prepare` followed by `bundle exec rspec spec`

## High-level Implementation/Approach

* `Films::ApiRequest` wraps the API requests. Upon requesting the root URL http://oscars.yipitdata.com/, the app then delegates `Films::Parser` to handle the bulk of the logic in requesting the winning films' data and parsing the JSON into the desired output format. Several edge cases are handled in the `#parse_budget` and `#parse_year` methods. Data is also cached in Redis (via format below) for faster subsequent retrievals (bypassing the n API requests for each year).

Redis key-value sample structure:
```
  "2010" => { "title" => "Title", "budget" => "1000" }
```
* The years were used as keys due to possible future implementation case of a user querying for a range of dates (pull data between year X and year Y). This allows for easier and more straightforward retrieval of the data associated with year from the cache

** Notes on Edge-Case Data

* Films with no budget listed were not factored into the average winning budget
* Budgets with a range (e.g. "$X-Y million") were calculated by average
* Dual years (e.g. 1932/33) were simplified to the first listed year (e.g. 1932)
* Different currencies are not accounted for. This could be accounted for using a regex unicode check and then using a `currency_multiplier` variable to adjust the budget

## Implementation Thoughts/Trade-offs

* The film data could be stored into a relational database (like SQL), which would allow for more complex queries. Doing so may also benefit cross-team members who are familiar with SQL querying, as they can directly interact with the data. A tradeoff would be that this might be unnecessary/expensive for simple data storage. Also, if a user must use a UI, the UI would need to be implemented to service those queries.

## Features

Users may:

* See all winning films and appropriate data in the provided Yipit API (navigate to root URL)
* See above data by directly requesting `/api/v1/films/get_films`
* See above data in a range of years if provided start and end years (script only)
* See Year-Title-Budget data in JSON format for a specific year by requesting from `/api/v1/films/get_film_for_year?year=#{your_year}` - this also helps with debugging
* See all cached dates
* Clear all cached dates

## Technologies

* **Back-End**: Rails 5 with a RESTful API. Redis on the caching layer
* **Notable Gems**: Redis
* **Front-End**: ReactJS to provide a more user-friendly experience and minimize requests to the server. Other node modules were also used (react-modal, etc)
* **Styling**: Semantic UI

## Possible Todos (interesting features):

* UI range of dates
